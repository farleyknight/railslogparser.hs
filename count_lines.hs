
import System.IO


main = do
  file <- openFile productionFile ReadMode
  hSetEncoding file utf8
  content <- hGetContents file
  putStrLn $ show $ length $ lines content


