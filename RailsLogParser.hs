
module RailsLogParser where

import RailsLogGrammar
import Char
import Data.Maybe
import System.IO
import Control.Applicative hiding ((<|>), many)
import Text.ParserCombinators.Parsec

import Control.Monad
import Database.HDBC
import Database.HDBC.MySQL
import Data.Ratio
import Data.List


maybeFirst :: Maybe (a, a) -> Maybe a
maybeFirst x =
  case x of
    Just (a, b) -> Just a
    Nothing     -> Nothing

maybeSecond :: Maybe (a, a) -> Maybe a
maybeSecond x =
  case x of
    Just (a, b) -> Just b
    Nothing     -> Nothing



-- NOTE (farleyknight@gmail.com): To future self, please find a more Haskell-ish
-- way to handle these two functions.. For now, I just want to get this working
maybeToString :: Show a => Maybe a -> String
maybeToString x =
  case x of
    Nothing -> ""
    Just y  -> show y

pString = do
  spaces
  string "P"
  p <- parametersString <|> processingString
  return p

railsLogParser = do
  accessString <|>
    pString <|>
    redirectedString <|>
    completedString

railsLogParser2 = do
  a  <- accessString
  p  <- processingString
  p' <- optionMaybe parametersString
  r  <- optionMaybe redirectedString
  c  <- completedString
  return (a, p, p', r, c)

runMaybeParser :: Parser a -> String -> Maybe a
runMaybeParser p input =
  case (parse p "" input) of
    Left error -> Nothing
    Right x    -> Just x


parseFile fileName parser = do
  file     <- openFile fileName ReadMode
  hSetEncoding file utf8
  contents <- hGetContents file
  let ls = lines contents in
    return (catMaybes (map (runMaybeParser parser) ls))

parseLogFile x = parseFile x railsLogParser

putStrEach xs = do
  mapM_ (\x -> putStrLn (show x)) xs

-- entriesOf fileName = do
--   return $ runMaybeParser fileName railsLogParser

takeNFromFile n fileName =
  (partEntries fileName) >>= return . (take n)
--  (parseLogFile fileName) >>= return . (take n)

showStuff = do
  entries <- parseLogFile "../log_files/sample_production.log"
  putStrEach entries

logsOf fileName = do
  logs <- partEntries fileName
  return (map logEntryFromPart logs)

partEntries fileName =
  parseLogFile fileName >>= return . partitionEntries'

partNEntries n fileName =
  parseLogFile fileName >>= pN
  where pN = return .
             (drop 1) .
             (take (n + 1)) .
             (map logEntryFromPart) .
             partitionEntries'

partitionEntries' entries = partitionEntries entries []

partitionEntries []      current = []

partitionEntries entries current =
  let e           = head entries
      restEntries = tail entries in

  case e of
    Access h u i d -> [current] ++ partitionEntries restEntries [e]
    _              -> partitionEntries restEntries (current ++ [e])


logEntryFromPart part = logEntryFromPart2 part blankLogEntry

logEntryFromPart2 part logEntry =
  if part == [] then
    logEntry
  else
    let p  = head part
        ps = tail part
    in
    case p of
      Access h u i d   -> logEntryFromPart2 ps logEntry { httpType = h,
                                                         url = u,
                                                         ipAddr = i,
                                                         timestamp = d }
      Processing c a f -> logEntryFromPart2 ps logEntry { controller = c,
                                                         action = a,
                                                         format = f }
      Parameters p     -> logEntryFromPart2 ps logEntry { parameters = p }
      Redirected r     -> logEntryFromPart2 ps logEntry { redirectedUrl = r }
      Completed r t ts -> logEntryFromPart2 ps logEntry { responseCode = r,
                                                         totalTime = show t,
                                                         viewTime = maybeToString (maybeFirst ts),
                                                         databaseTime = maybeToString (maybeSecond ts) }


                       -- Completed String Float (Maybe (Float, Float))

data RailsLogEntry = RailsLogEntry {
  httpType      :: String,
  url           :: String,
  ipAddr        :: String,
  timestamp     :: String,
  controller    :: String,
  action        :: String,
  format        :: String,
  parameters    :: String,
  redirectedUrl :: String,
  responseCode  :: String,
  totalTime     :: String,
  viewTime      :: String,
  databaseTime  :: String
  } deriving Show


blankLogEntry = RailsLogEntry {
  httpType      = undefined,
  url           = undefined,
  ipAddr        = undefined,
  timestamp     = undefined,
  controller    = "",
  action        = "",
  format        = "",
  parameters    = "",
  redirectedUrl = "",
  responseCode  = "",
  totalTime     = "",
  viewTime      = "",
  databaseTime  = ""
  }


mysqlConnection =
  connectMySQL defaultMySQLConnectInfo {
    mysqlHost       = "localhost",
    mysqlUser       = "guest",
    mysqlPassword   = "password",
    mysqlDatabase   = "requests",
    mysqlUnixSocket = "/var/run/mysqld/mysqld.sock"
    }

runDb = do
  rows <- withRTSSignalsBlocked $ do
    conn <- mysqlConnection
    quickQuery' conn "SELECT 1 + 1" []
  forM_ rows $ \row -> putStrLn $ show row


insertIntoDb = do
  conn <- mysqlConnection
  run conn ("INSERT INTO requests (http_type, url, ip_address, timestamp) VALUES " ++ values) []
  commit conn
  where values = ""

toSQL :: RailsLogEntry -> String
toSQL entry = show tuple
  where
    tuple = (httpType entry, url entry, ipAddr entry, timestamp entry)

toJSON :: RailsLogEntry -> String
toJSON entry = "{" ++
               "  http_type: "      ++ (httpType entry) ++
               ", url: "            ++ (url entry) ++
               ", ip_address: "     ++ (ipAddr entry) ++
               ", timestamp: "      ++ (timestamp entry) ++
               ", controller: "     ++ (controller entry) ++
               ", action: "         ++ (action entry) ++
               ", format: "         ++ (format entry) ++
               ", parameters: "     ++ (parameters entry) ++
               ", redirected_url: " ++ (redirectedUrl entry) ++
               ", response_code: "  ++ (responseCode entry) ++
               ", total_time: "     ++ (totalTime entry) ++
               ", view_time: "      ++ (viewTime entry) ++
               ", database_time: "  ++ (databaseTime entry) ++
               "}"



-- Given a file, segment into n more files, in such a way that
-- each segment starts with a "Started GET" or "Started POST"
-- entry, preventing entries from being lost.


countLinesFrom fileName = do
  file <- openFile fileName ReadMode
  hSetEncoding file utf8
  countLines file 0

-- Find the # of lines in the file, using as little memory as we can

countLines file k = do
  end <- hIsEOF file
  if end
    then do
    return $ k
    else do
    l <- hGetLine file
    showK k
    countLines file (k + 1)

  where
    showK k = do
      if ((k `mod` 100) == 0) && (k /= 0)
        then putStrLn (show k)
        else return ()


productionFile       = "../log_files/production.log"
sampleProductionFile = "../log_files/sample_production.log"


logDir = "logs"

-- linesPerFile = 100 * 1000

firstStartingLine linesPerFile fileName = do
  h <- openFile fileName ReadMode
  g <- openNextFile 0
  hSetEncoding h utf8
  process (h, g) 0 []
  where
    process (h, g) k lines = do
      h' <- hTakeLines (h, g) linesPerFile 0
      end <- hIsEOF h
      if end
        then return lines
        else do
        (n', line) <- loop (h, g) k'
        g' <- openNextFile $ (length lines) + 1
        hPutStrLn g' line
        process (h, g') n' $ lines ++ [n']
        where
          k' = k + linesPerFile

    openNextFile n = do
      g <- openFile name WriteMode
      hSetEncoding g utf8
      return g
      where
        name = "logs/segment" ++ (show n) ++ ".log"

    hTakeLines (h, g) n i = do
      end <- hIsEOF h
      if end
        then return h
        else do
        hGetLine h >>= (\l -> hPutStrLn g l)
        if (n == i)
          then return h
          else hTakeLines (h,g) n (i + 1)

    loop (h, g) n = do
      end <- hIsEOF h
      if end
        then return (n, "")
        else do
        line <- hGetLine h
        if "Started" `isPrefixOf` line
          then do
          putStrLn $ show $ (n, line)
          return (n, line)
          else do
          hPutStrLn g line
          loop (h,g) (n + 1)


