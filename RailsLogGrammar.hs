
module RailsLogGrammar where

import Char
import Text.ParserCombinators.Parsec


data RailsLogExample = Access String String String String |
                       Processing String String String |
                       Parameters String |
                       Redirected String |
                       Completed String Float (Maybe (Float, Float))
                       deriving (Show, Eq)


float = do
  n <- many1 digit
  d <- optionMaybe decimal
  return (n ++ maybeStringToString d)


maybeStringToString :: Maybe String -> String
maybeStringToString x =
  case x of
    Nothing -> ""
    Just y  -> y


ipAddress :: Parser String
ipAddress = do
  first <- many1 digit
  string "."
  second <- many1 digit
  string "."
  third  <- many1 digit
  string "."
  fourth <- many1 digit
  return (first ++ "." ++ second ++ "." ++ third ++ "." ++ fourth)

intOrDecimal :: Parser String
intOrDecimal = many1 $ oneOf "0123456789_"

timezone :: Parser String
timezone = do
  pm <- ((string "+") <|> (string "-"))
  hh <- many1 digit
  return (pm ++ hh)

dateTime :: Parser String
dateTime = do
  year    <- many1 digit
  string "-"
  month   <- many1 digit
  string "-"
  date    <- many1 digit
  string " "
  hour    <- many1 digit
  string ":"
  minute  <- many1 digit
  string ":"
  seconds <- many1 digit
  string " "
  tz      <- timezone
  return (year ++ "-" ++ month ++ "-" ++ date ++ " " ++ hour ++ ":" ++ minute ++ ":" ++ seconds ++ " " ++ tz)


camelCase :: Parser Char
camelCase = satisfy (\x -> isAlpha x)

lowerCase :: Parser Char
lowerCase = satisfy (\x -> isAlpha x && isLower x)

upperCase :: Parser Char
upperCase = satisfy (\x -> isAlpha x && isUpper x)

restOfLine :: Parser String
restOfLine = many (noneOf "\n")



-- Parse a quoted string
-- Ex: "something"
quotedString :: Parser String
quotedString = do
  char '"'
  p <- many (noneOf "\"")
  char '"'
  return p


fiveHundred :: Parser String
fiveHundred = string "500 Internal Server Error"

threeOhTwo :: Parser String
threeOhTwo  = string "302 Found"

twoHundred :: Parser String
twoHundred  = string "200 OK"

decimal :: Parser String
decimal = do
  string "."
  x <- many1 digit
  return ("." ++ x)


renderTime = do
  f <- float
  string "ms"
  return (read f :: Float)

extraTimes = do
  string "(Views: "
  a <- renderTime
  string " | ActiveRecord: "
  b <- renderTime
  string ")"
  return (a, b)

accessExample = "Started GET \"/\" for 71.225.51.40 at 2011-10-30 19:37:56 +0300"

accessString :: Parser RailsLogExample
accessString = do
  string "Started "
  h <- ((string "GET") <|> (string "POST"))
  string " "
  u <- quotedString
  string " for "
  i <- ipAddress
  string " at "
  d <- dateTime
  return (Access h u i d)

processingExample = "  Processing by MainController#index as HTML"

processingString :: Parser RailsLogExample
processingString = do
  string "rocessing by "
  c <- many1 camelCase
  string "#"
  a <- many1 lowerCase
  return (c, a)
  string " as "
  f <- many1 upperCase
  return (Processing c a f)


parametersExample = "  Parameters: {\"utf8\"=>\"✓\", \"authenticity_token\"=>\"Kv9VbWsDbBQHeFYPcWK6gqQ/MtUTh7RasZDMjkSbpnw=\", \"login\"=>\"farleyknight@gmail.com\", \"password\"=>\"[FILTERED]\", \"commit\"=>\"Submit\"}"
parametersString :: Parser RailsLogExample
parametersString = do
  string "arameters: "
  p <- restOfLine
  return (Parameters p)

redirectedExample = "Redirected to https://www2.gbrecycle.com/admin/users"
redirectedString :: Parser RailsLogExample
redirectedString = do
  string "Redirected to "
  p <- restOfLine
  return (Redirected p)


completedExample1 = "Completed 500 Internal Server Error in 46ms"
completedExample2 = "Completed 302 Found in 37ms"
completedExample3 = "Completed 200 OK in 3389ms (Views: 2967.9ms | ActiveRecord: 369.0ms)"

completedString = do
  string "Completed "
  p <- fiveHundred <|> threeOhTwo <|> twoHundred
  string " in "
  r <- renderTime
  spaces
  t <- optionMaybe extraTimes
  return (Completed p r t)

